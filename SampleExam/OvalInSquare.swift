//
//  OvalInSquare.swift
//  SampleExam
//
//  Created by Administrator on 2016-04-12.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit

class OvalInSquare: Square {
    
    let ovalColor:[CGFloat]
    
    init (X:CGFloat, Y:CGFloat, size:CGFloat, ovalColor: [CGFloat])
    {
        self.ovalColor = ovalColor
        super.init(X: X, Y: Y, size: size)
    }
    
    override func draw(theContext:CGContext) {
        let rect = CGRect(x: self.X, y: self.Y, width: self.size, height: self.size)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let color = CGColorCreate(colorSpace, self.ovalColor)
        CGContextSetFillColorWithColor(theContext, color)
        CGContextAddEllipseInRect(theContext, rect)
        CGContextFillPath(theContext);
        
        CGContextAddRect(theContext, rect)
        CGContextStrokePath(theContext);
        

    }

}