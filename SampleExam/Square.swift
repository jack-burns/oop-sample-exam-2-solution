//
//  Square.swift
//  SampleExam
//
//  Created by Administrator on 2016-04-12.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit

class Square {
    var X:CGFloat
    var Y:CGFloat
    var size:CGFloat
    
    init (X:CGFloat, Y:CGFloat, size:CGFloat)
    {
        self.X = X
        self.Y = Y
        self.size = size
    }
    
    func draw(theContext:CGContext) {}
    
    func move(deltaX:CGFloat, deltaY:CGFloat) {
        self.X += deltaX
        self.Y += deltaY
    }
}
