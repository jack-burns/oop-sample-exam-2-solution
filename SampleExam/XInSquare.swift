//
//  XInSquare.swift
//  SampleExam
//
//  Created by Administrator on 2016-04-12.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit

class XInSquare: Square {
    
    let thickness:CGFloat
    
    init (X:CGFloat, Y:CGFloat, size:CGFloat, thickness: CGFloat) {
        self.thickness = thickness
        super.init(X: X, Y: Y, size: size)
    }
    
    override func draw(theContext:CGContext) {
        
        let rect = CGRect(x: self.X, y: self.Y, width: self.size, height: self.size)
        CGContextSetLineWidth(theContext, self.thickness)
        CGContextMoveToPoint(theContext, self.X, self.Y);
        CGContextAddLineToPoint(theContext, self.X+self.size, self.Y+self.size)
        CGContextMoveToPoint(theContext, self.X+self.size, self.Y);
        CGContextAddLineToPoint(theContext, self.X, self.Y+self.size)
        CGContextStrokePath(theContext);

        CGContextSetLineWidth(theContext, 1)
        CGContextAddRect(theContext, rect)
        CGContextStrokePath(theContext);
    }
    
}
