//
//  DetailedView.swift
//  SampleExam
//
//  Created by Administrator on 2016-04-12.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit

class DetailedView: UIView {
    
    var squares = [Square]()

    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 1.0)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let components: [CGFloat] = [0.0, 0.0, 1.0, 1.0]
        let color = CGColorCreate(colorSpace, components)
        CGContextSetStrokeColorWithColor(context, color)
        
        for i in 0..<squares.count {
            squares[i].draw(context!)
        }
    }


}
