//
//  ViewController.swift
//  SampleExam
//
//  Created by Administrator on 2016-04-12.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let constMoveDelta:CGFloat = 5
    
    @IBAction func addOval(sender: AnyObject) {
        theDetailedView.squares.append(OvalInSquare(X: 20, Y: 20, size: 100, ovalColor: [1.0,0,0,1.0]))
        enableButtons()
        theDetailedView.setNeedsDisplay()
    }

    @IBAction func addX(sender: AnyObject) {
        theDetailedView.squares.append(XInSquare(X: 20, Y: 20, size: 100, thickness: 2))
        enableButtons()
        theDetailedView.setNeedsDisplay()
    }
    
    @IBOutlet weak var left: UIBarButtonItem!
    @IBAction func moveLeft(sender: AnyObject) {
        theDetailedView.squares.last!.move(-constMoveDelta,deltaY: 0)
        theDetailedView.setNeedsDisplay()
    }
    
    @IBOutlet weak var right: UIBarButtonItem!
    @IBAction func moveRight(sender: AnyObject) {
        theDetailedView.squares.last!.move(constMoveDelta,deltaY: 0)
        theDetailedView.setNeedsDisplay()
    }
    
    @IBOutlet weak var up: UIBarButtonItem!
    @IBAction func moveUp(sender: AnyObject) {
        theDetailedView.squares.last!.move(0,deltaY: -constMoveDelta)
        theDetailedView.setNeedsDisplay()
    }
    
    @IBOutlet weak var down: UIBarButtonItem!
    @IBAction func moveDown(sender: AnyObject) {
        theDetailedView.squares.last!.move(0,deltaY: constMoveDelta)
        theDetailedView.setNeedsDisplay()
    }
    

    @IBOutlet weak var theDetailedView: DetailedView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func enableButtons(){
        left.enabled = true
        right.enabled = true
        up.enabled = true
        down.enabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

